package com.marekCompany.blogTest.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.GroupSequence;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.marekCompany.blogTest.validator.CheckUniqueness;
import com.marekCompany.blogTest.validator.GroupCheck.Insert;
import com.marekCompany.blogTest.validator.GroupCheck.Update;
import com.marekCompany.blogTest.validator.GroupCheck.Last;

@Entity
@Table(name = "author")
@GroupSequence({ Insert.class, Update.class, Author.class, Last.class })
@CheckUniqueness(message = "Username already exists", groups = { Last.class })
public class Author implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@NotNull(groups = Update.class, message = "Something went wrog. Author id is missing")
	@Column(name = "author_id")
	private Long id;

	@NotEmpty(message = "This field is required", groups = Insert.class)
	@Length(min = 5, max = 50, message = "Username must have between 5 and 50 characters", groups = { Update.class, Insert.class })
	
	@Column(name = "username")
	private String username;

	@JsonProperty(access = Access.WRITE_ONLY)
	@NotEmpty(message = "This field is required", groups = Insert.class)
	@Length(min = 5, max = 16, message = "Your password must have at least 5 characters", groups = { Update.class, Insert.class })
	@Column(name = "password")
	private String password;

	@NotEmpty(message = "This field is required", groups = Insert.class)
	@Length(min = 5, max = 50, message = "Nickname must have between 5 and 50 characters", groups = { Update.class, Insert.class })
	@Column(name = "nickname")
	private String nickname;

	@Column(name = "registered")
	private Timestamp registered;

	@Email(message = "Fill valid email", groups = { Update.class, Insert.class })
	@NotEmpty(message = "This field is required", groups = Insert.class)
	@Column(name = "email")
	private String email;

	@Column(name = "enabled")
	private int enabled;

	@ManyToMany
	@JoinTable(name = "author_role", joinColumns = @JoinColumn(name = "author_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
	private Set<Role> roles;

	
	public Long getId() {
		return id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@JsonIgnore
	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public Timestamp getRegistered() {
		return registered;
	}

	public void setRegistered(Timestamp registered) {
		this.registered = registered;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getEnabled() {
		return enabled;
	}

	public void setEnabled(int enabled) {
		this.enabled = enabled;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public void createFirstPerson() {
		// TODO Auto-generated method stub
		
	}
}