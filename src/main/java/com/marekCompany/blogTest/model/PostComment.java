package com.marekCompany.blogTest.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="post_comment")
public class PostComment implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "comment_id")
	private Long id;

	@Column(name = "parent_comment_id")
	private Long parentId;
	
	@Column(name = "post_id")
	private Long postId;
	
	@Column(name = "created")
	private Timestamp created;

	@Column(name = "author_email")
	private String author_email;
	
	@Column(name = "author_name")
	private String author_name;
	
	@Column(name = "content")
	private String content;
	
	
}
