package com.marekCompany.blogTest.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.GroupSequence;
import javax.validation.constraints.NotNull;
import javax.validation.groups.Default;

import org.hibernate.validator.constraints.Length;

import com.marekCompany.blogTest.PostStatus;
import com.marekCompany.blogTest.validator.CheckCategoryExistence;
import com.marekCompany.blogTest.validator.CheckExistence;
import com.marekCompany.blogTest.validator.CheckUniqueness;
import com.marekCompany.blogTest.validator.GroupCheck.Insert;
import com.marekCompany.blogTest.validator.GroupCheck.Update;
import com.marekCompany.blogTest.validator.GroupCheck.Last;

@Entity
@Table(name = "post")
@GroupSequence({ Insert.class, Update.class, Post.class, Last.class })
@CheckUniqueness(message = "Url name already exists", groups = { Last.class })
public class Post implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@NotNull(groups = Update.class, message = "Something went wrog. Post id is missing.")
	@Column(name = "post_id")
	private Long id;

	@ManyToOne
	@JoinColumn(name = "author_id")
	@NotNull(message = "Something went wrog. Auhor is missing.", groups = { Insert.class, Update.class })
	private Author author;

	@Column(name = "created")
	private Timestamp created;

	@Column(name = "last_update")
	private Timestamp lastUpdate;

	@Length(min = 5, max = 500, message = "Length must be between 5 and 500.", groups = { Insert.class, Update.class })
	@Column(name = "description")
	private String description;

	@Column(name = "content")
	private String content;

	@Length(min = 5, max = 50, message = "Length must be between 5 and 50.", groups = { Insert.class, Update.class })
	@Column(name = "title")
	private String title;

	@Length(min = 5, max = 100, message = "Length must be between 5 and 100.", groups = { Insert.class, Update.class })
	@Column(name = "url_name")
	private String urlName;
	
	@Column(name = "cover_img_url")
	private String coverImgUrl;

	@Enumerated(EnumType.STRING)
	@Column(name = "status")
	private PostStatus status;

	@ManyToMany
	@CheckCategoryExistence(message = "Post contains unknown category")
	@NotNull(message = "This post needs to be in at least one category", groups = { Insert.class, Update.class })
	//@CheckExistence(message = "Category doesn't exist", groups = { Insert.class, Update.class })
	@JoinTable(name = "post_category", joinColumns = @JoinColumn(name = "post_id"), inverseJoinColumns = @JoinColumn(name = "category_id"))
	private Set<Category> categories;

	@OneToMany(mappedBy = "post", cascade = CascadeType.REMOVE)
	private Set<PostView> postViews;

	public Long getId() {
		return id;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public Timestamp getCreated() {
		return created;
	}

	public void setCreated(Timestamp created) {
		this.created = created;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public PostStatus getStatus() {
		return status;
	}

	public void setStatus(PostStatus status) {
		this.status = status;
	}

	public Set<Category> getCategories() {
		return categories;
	}

	public void setCategories(Set<Category> categories) {
		this.categories = categories;
	}

	public String getUrlName() {
		return urlName;
	}

	public void setUrlName(String urlName) {
		this.urlName = urlName;
	}

	public Timestamp getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Timestamp lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public Set<PostView> getPostViews() {
		return postViews;
	}
}
