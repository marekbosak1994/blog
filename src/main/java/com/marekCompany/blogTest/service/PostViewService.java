package com.marekCompany.blogTest.service;

import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.marekCompany.blogTest.model.Post;
import com.marekCompany.blogTest.model.PostView;
import com.marekCompany.blogTest.repository.PostViewRepository;

@Service("postViewService")
public class PostViewService {

	@Autowired
	private PostViewRepository postViewRepository;

	public void deleteViewsByPostId(Long postId) {
		List<PostView> postViews = postViewRepository.findByPostId(postId);
		postViewRepository.delete(postViews);
	}

	public void savePostView(Post post, String cookie) {
		Date currentDate = new Date(System.currentTimeMillis());

		if (isPostViewAlreadyExists(post, cookie, currentDate)) {
			return;
		}

		PostView postView = new PostView();
		postView.setCookie(cookie);
		postView.setPost(post);
		postView.setViewed(currentDate);

		postViewRepository.save(postView);

	}

	private boolean isPostViewAlreadyExists(Post post, String cookie, Date currentDate) {
		List<PostView> listOfExistingPostViews = postViewRepository.findByPostAndCookieAndViewed(post, cookie,
				currentDate);
		if (listOfExistingPostViews == null || listOfExistingPostViews.isEmpty()) {
			return false;
		} else {
			return true;
		}
	}

	/*
	 * public int countPostViewsByPostId(Long postId) { return
	 * postViewRepository.countByPost(postId); }
	 */

}
