package com.marekCompany.blogTest.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.marekCompany.blogTest.exception.NotFoundException;
import com.marekCompany.blogTest.model.Category;
import com.marekCompany.blogTest.model.Post;
import com.marekCompany.blogTest.repository.CategoryRepository;

@Service("categoryService")
public class CategoryService implements BaseService<Category, Long> {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(CategoryService.class);

	@Autowired
	private CategoryRepository categoryRepository;

	@Autowired
	PostService postService;
	
	@Override
	public Category save(Category category) {
		return categoryRepository.save(category);
	}

	@Override
	public List<Category> getAll() {
		return categoryRepository.findAll();
	}

	@Override
	public Category update(Category entity) {
		return categoryRepository.save(entity);
	}

	@Override
	public void delete(Long key) {
		if(!exists(key)) {
			throw new NotFoundException("Category not found");
		}
		//TODO check if any posts contain this category
		List<Post> posts = postService.findByCategory(key);
		if(posts != null && !posts.isEmpty()) {
			LOGGER.info("There are posts associated with author: " + key);
			throw new NotFoundException("There are posts associated with author");//"there are posts associated with this category"
		}
		categoryRepository.delete(key);
	}

	@Override
	public Category findByKey(Long key) {
		if (!exists(key)) {
			throw new NotFoundException("Category not found");
		}
		return categoryRepository.findOne(key);
	}

	public Category findByName(String name) {
		return categoryRepository.findByName(name);
	}

	@Override
	public boolean exists(Long key) {
		return categoryRepository.exists(key);
	}

}
