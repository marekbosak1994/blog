package com.marekCompany.blogTest.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.marekCompany.blogTest.exception.BadRequestException;
import com.marekCompany.blogTest.exception.NotFoundException;
import com.marekCompany.blogTest.model.Author;
import com.marekCompany.blogTest.model.Post;
import com.marekCompany.blogTest.repository.AuthorRepository;

@Service("authorService")
@Transactional
public class AuthorService implements BaseService<Author, Long> {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(AuthorService.class);
	
	@Autowired
	PostService postService;
	
	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	private AuthorRepository authorRepository;


	public Author findByUsername(String username) {
		return authorRepository.findByUsername(username);
	}

	@Override
	public Author save(Author author) {
		return authorRepository.save(author);
	}

	@Override
	public Author update(Author authorWithNewData) {
		Long authorId = authorWithNewData.getId();

		Author authorToUpdate = findByKey(authorId);

		if (isNotNullAndNotEmpty(authorWithNewData.getEmail())) {
			authorToUpdate.setEmail(authorWithNewData.getEmail());
		}
		if (isNotNullAndNotEmpty(authorWithNewData.getNickname())) {
			authorToUpdate.setNickname(authorWithNewData.getNickname());
		}
		if (isNotNullAndNotEmpty(authorWithNewData.getUsername())) {
			authorToUpdate.setUsername(authorWithNewData.getUsername());
		}
		
		authorToUpdate.setRoles(authorWithNewData.getRoles());
		authorToUpdate.setEnabled(authorWithNewData.getEnabled());

		return authorRepository.save(authorToUpdate);
	}

	private static boolean isNotNullAndNotEmpty(String stringToCheck) {
		return stringToCheck != null && !stringToCheck.isEmpty();
	}

	@Override
	public void delete(Long authorId) {
		if (!exists(authorId)) {
			LOGGER.info("No author found with id: " + authorId);
			throw new NotFoundException("Author not found");
		}
		
		List<Post> posts = postService.findByAuthorId(authorId);
		if(posts != null && !posts.isEmpty()) {
			LOGGER.info("There are posts associated with author");
			throw new BadRequestException("There are posts associated with author"); //TODO create badrequest exception
		}
		
		authorRepository.delete(authorId);
	}

	@Override
	public Author findByKey(Long key) {
		if (!exists(key)) {
			throw new NotFoundException("Author not found");
		}
		return authorRepository.findOne(key);
	}

	@Override
	public List<Author> getAll() {
		return authorRepository.findAll();
	}

	public boolean isAuthorExists(String username) {
		return findByUsername(username) != null;
	}

	public boolean isUsernameUnique(String username) {
		return findByUsername(username) == null;
	}

	@Override
	public boolean exists(Long key) {
		return authorRepository.exists(key);
	}
}
