package com.marekCompany.blogTest.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.ValidatorFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.marekCompany.blogTest.model.Post;
import com.marekCompany.blogTest.PostStatus;
import com.marekCompany.blogTest.exception.NotFoundException;
import com.marekCompany.blogTest.model.Author;
import com.marekCompany.blogTest.repository.PostRepository;

@Service("postService")
public class PostService implements BaseService<Post, Long> {

	private static final Logger LOGGER = LoggerFactory.getLogger(PostService.class);

	@Autowired
	private PostRepository postRepository;

	@Autowired
	PostViewService postViewService;

	@Autowired
	AuthorService authorService;

	@Autowired
	CategoryService categoryService;

	@Autowired
	private ValidatorFactory factory;

	@Override
	public Post save(Post post) {
		Long authorId = post.getAuthor().getId();
		if (!authorService.exists(authorId)) {
			LOGGER.info("save() - authorId not found: " + authorId);
			throw new NotFoundException("While saving author not found");
		}
		List<Long> categoryIds = post.getCategories().stream().map(c -> c.getId()).collect(Collectors.toList());
		for (Long id : categoryIds) {
			if (!categoryService.exists(id)) {
				LOGGER.info("save() - categoryId not found: " + id);
				throw new NotFoundException("while saving category not found");
			}
		}
		post.setCreated(new Timestamp(System.currentTimeMillis()));

		return postRepository.save(post);
	}

	@Override
	public Post update(Post post) {
		post.setLastUpdate(new Timestamp(System.currentTimeMillis()));
		return save(post);
	}

	@Override
	public List<Post> getAll() {
		return postRepository.findAll();
	}
	
	public List<Post> getAllOrderedByCreatedAsc(){
		return postRepository.findAll(new Sort(Direction.DESC, "id"));
	}
	
	public List<Post> getByStatus(String status) {
		status = status.toUpperCase();
		if(PostStatus.PUBLISHED.toString().toUpperCase().equals(status)) {
			return postRepository.findByStatusOrderByCreatedAsc(PostStatus.PUBLISHED);
		}else if(PostStatus.DRAFT.toString().toUpperCase().equals(status)){
			return postRepository.findByStatusOrderByCreatedAsc(PostStatus.DRAFT);
		}else if(PostStatus.ARCHIVED.toString().toUpperCase().equals(status)) {
			return postRepository.findByStatusOrderByCreatedAsc(PostStatus.ARCHIVED);
		}else if("ALL".equals(status)) {
			return getAllOrderedByCreatedAsc();
		}
		
		return new ArrayList<>();
	}
	
	public List<Post> getAllPublished(){
		return postRepository.findByStatusOrderByCreatedAsc(PostStatus.PUBLISHED);
	}

	@Override
	public void delete(Long postId) {
		if (!exists(postId)) {
			LOGGER.info("delete() - postId not found: " + postId);
			throw new NotFoundException("Post not found");
		}

		postRepository.delete(postId);
	}

	@Override
	public Post findByKey(Long postId) {
		if (!exists(postId)) {
			throw new NotFoundException("Post not found");
		}

		return postRepository.findOne(postId);
	}

	public List<Post> findByAuthorId(Long authorId) {
		return postRepository.findByAuthorId(authorId);
	}

	public List<Post> findByCategory(Long categoryId) {
		return postRepository.findByCategoriesId(categoryId);
	}

	@Override
	public boolean exists(Long key) {
		return postRepository.exists(key);
	}

	public boolean isUrlNameUnique(String urlName) {
		Post post = postRepository.findByUrlName(urlName);
		return post == null;
	}

	public Post findByUrlName(String urlName) {
		return postRepository.findByUrlName(urlName);
	}
}
