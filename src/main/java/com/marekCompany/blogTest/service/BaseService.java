package com.marekCompany.blogTest.service;

import java.util.List;

public interface BaseService<E, K> {
	public E save(E entity);

	public E update(E entity);

	public void delete(K key);

	public E findByKey(K key);

	public List<E> getAll();
	
	public boolean exists(K key);
}
