package com.marekCompany.blogTest.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.marekCompany.blogTest.model.PostComment;
import com.marekCompany.blogTest.repository.PostCommentRepository;

@Service("postCommentService")
public class PostCommentSerivce implements BaseService<PostComment, Long> {

	@Autowired
	private PostCommentRepository postCommentRepository;

	@Override
	public PostComment save(PostComment entity) {
		return postCommentRepository.save(entity);
	}

	@Override
	public PostComment update(PostComment entity) {
		return postCommentRepository.save(entity);
	}

	@Override
	public void delete(Long key) {
		postCommentRepository.delete(key);
	}

	@Override
	public PostComment findByKey(Long key) {
		return postCommentRepository.getOne(key);
	}

	@Override
	public List<PostComment> getAll() {
		return postCommentRepository.findAll();
	}

	public PostComment findByPost(Long postId) {
		return postCommentRepository.getOne(postId);
	}

	@Override
	public boolean exists(Long key) {
		// TODO Auto-generated method stub
		return false;
	}

}
