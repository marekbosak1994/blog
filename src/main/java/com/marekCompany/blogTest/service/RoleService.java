package com.marekCompany.blogTest.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.marekCompany.blogTest.exception.NotFoundException;
import com.marekCompany.blogTest.model.Role;
import com.marekCompany.blogTest.repository.RoleRepository;

@Service("roleService")
public class RoleService implements BaseService<Role, Long> {

	private static final Logger LOGGER = LoggerFactory.getLogger(RoleService.class);

	@Autowired
	private RoleRepository roleRepository;

	@Override
	public Role save(Role role) {
		return roleRepository.save(role);
	}

	@Override
	public List<Role> getAll() {
		return roleRepository.findAll();
	}

	@Override
	public Role update(Role role) {
		return roleRepository.save(role);
	}

	@Override
	public void delete(Long key) {
		if (!exists(key)) {
			throw new NotFoundException("Role not found");
		}
		roleRepository.delete(key);
	}

	@Override
	public Role findByKey(Long key) {
		if (!exists(key)) {
			throw new NotFoundException("Role not found");
		}
		return roleRepository.findOne(key);
	}

	@Override
	public boolean exists(Long key) {
		return roleRepository.exists(key);
	}

}
