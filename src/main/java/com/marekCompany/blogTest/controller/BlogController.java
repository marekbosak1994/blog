package com.marekCompany.blogTest.controller;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.marekCompany.blogTest.model.Category;
import com.marekCompany.blogTest.model.Post;
import com.marekCompany.blogTest.model.Role;
import com.marekCompany.blogTest.model.Author;
import com.marekCompany.blogTest.service.CategoryService;
import com.marekCompany.blogTest.service.PostService;
import com.marekCompany.blogTest.service.PostViewService;
import com.marekCompany.blogTest.service.RoleService;
import com.marekCompany.blogTest.service.AuthorService;

//@CrossOrigin(origins = "*", allowCredentials = "true", exposedHeaders = "Authorization")
@RestController
public class BlogController {

	private static final Logger LOGGER = LoggerFactory.getLogger(BlogController.class);
	//private static String UPLOAD_PATH = "D:\\JavaEE\\blog\\src\\main\\resources\\static\\images\\";
	private static String UPLOAD_PATH = System.getProperty("user.dir") + "\\src\\main\\resources\\static\\images\\";
	private static final String RETRIEVE_PATH = "http:\\\\localhost:8090\\";

	@Autowired
	AuthorService authorService;

	@Autowired
	PostService postService;

	@Autowired
	PostViewService postViewService;

	@Autowired
	RoleService roleService;

	@Autowired
	CategoryService categoryService;

	@RequestMapping(value = "/images", method = RequestMethod.POST)
	public ResponseEntity<String> uploadCoverImageForPost(@RequestParam("file") MultipartFile uploadfile) {
		if (uploadfile.isEmpty()) {
			LOGGER.info("file is empty");
			return new ResponseEntity<String>("", HttpStatus.BAD_REQUEST);
		}

		try {
			LOGGER.info("uploading...");
			String folderPath = saveUploadedFiles(uploadfile);
			
			
			int imagesIndex = folderPath.toString().indexOf("images");
			
			String urlPath = RETRIEVE_PATH + folderPath.toString().substring(imagesIndex);
			urlPath = urlPath.replace("\\", "/");
			
			LOGGER.info("upload success, folderPath: " + folderPath);
			LOGGER.info("upload success, urlPath: " + urlPath);
			
			JSONObject json = new JSONObject();
			json.put("link", urlPath);
			return new ResponseEntity<String>(json.toString(), HttpStatus.OK);
		} catch (Exception e) {
			LOGGER.info("failed while uploading...");
			e.printStackTrace();
			return new ResponseEntity<String>("", HttpStatus.BAD_REQUEST);
		}

	}

	private String saveUploadedFiles(MultipartFile file) throws Exception {
		if (file.isEmpty()) {
			throw new Exception(); // next pls
		}

		String fileName = file.getOriginalFilename().replace(" ", "_"); // get filename as string and replace white spaces with underscores
		Path fullPath = Paths.get(UPLOAD_PATH + fileName);				// e.g. D:\JavaEE\blog\src\main\resources\static\images\picture.jpg
		
		byte[] bytes = file.getBytes();
		Files.write(fullPath, bytes);
		
		return fullPath.toString();
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<HttpStatus> login() {
		LOGGER.info("Login successfuly");
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@RequestMapping(value = "user", method = RequestMethod.POST)
	public ResponseEntity<Author> createFirstUser(@RequestBody Author author){ 
		Role adminRole = new Role();
		adminRole.setRole("ADMIN");
		Role savedROle = roleService.save(adminRole);
		Set<Role> roles = new LinkedHashSet<>();
		roles.add(savedROle);
		author.setRoles(roles);
		authorService.save(author);
		return new ResponseEntity<Author>(HttpStatus.OK);
	}

	@RequestMapping(value = "/posts", method = RequestMethod.GET)
	public ResponseEntity<List<Post>> getAllPublishedPosts() {
		LOGGER.info("getAllPublishedPosts() GET - start");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		List<Post> posts = postService.getAllPublished();
		LOGGER.info(UPLOAD_PATH);
		LOGGER.info("getAllPublishedPosts() GET - end");
		return new ResponseEntity<List<Post>>(posts, HttpStatus.OK);
	}

	@RequestMapping(value = "/post/{postId}", method = RequestMethod.GET) // TODO only published posts
	public ResponseEntity<Post> getPostById(@PathVariable Long postId,
			@CookieValue(value = "JSESSIONID", required = false) String cookie) {
		LOGGER.info("getPostById() GET - start");
		LOGGER.info("getPostById() GET - postid: " + postId);
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		Post post = postService.findByKey(postId);
		postViewService.savePostView(post, cookie);

		LOGGER.info("getPostById() GET - post with id: " + postId + " successfuly returned");
		LOGGER.info("getPostById() GET - end");
		return new ResponseEntity<Post>(post, HttpStatus.OK);
	}

	@RequestMapping(value = "/author/{username}/posts", method = RequestMethod.GET)
	public ResponseEntity<List<Post>> getPostsByAuthor(@PathVariable String username) {
		LOGGER.info("getPostsByAuthor() GET - start");
		LOGGER.info("getPostsByAuthor() GET - username: " + username);
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		Author author = authorService.findByUsername(username);
		List<Post> posts = postService.findByAuthorId(author.getId());

		LOGGER.info("getPostsByAuthor() GET - list of posts filtred by author successfuly returned");
		LOGGER.info("getPostsByAuthor() GET - end");
		return new ResponseEntity<List<Post>>(posts, HttpStatus.OK);
	}

	@RequestMapping(value = "/category/{categoryId}/posts", method = RequestMethod.GET)
	public ResponseEntity<List<Post>> getPostsByCategory(@PathVariable Long categoryId) {
		LOGGER.info("getPostsByCategory() GET - start");
		LOGGER.info("getPostsByCategory() GET - categoryid: " + categoryId);
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		List<Post> posts = postService.findByCategory(categoryId);

		LOGGER.info("getPostsByCategory() GET - list of posts filtred by category successfuly returned");
		LOGGER.info("getPostsByCategory() GET - end");
		return new ResponseEntity<List<Post>>(posts, HttpStatus.OK);
	}

	@RequestMapping(value = "/authors", method = RequestMethod.GET)
	public ResponseEntity<List<Author>> getAllAuthors() {
		LOGGER.info("getAllAuthors() GET - start");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		List<Author> allAuthors = authorService.getAll();

		LOGGER.info("getAllAuthors() GET - list of authors successfuly returned");
		LOGGER.info("getAllAuthors() GET - end");
		return new ResponseEntity<List<Author>>(allAuthors, HttpStatus.OK);
	}

	@RequestMapping(value = "/author/{authorId}", method = RequestMethod.GET)
	public ResponseEntity<Author> getAuthorById(@PathVariable Long authorId) {
		LOGGER.info("getAuthorById() GET - start");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		Author author = authorService.findByKey(authorId);

		LOGGER.info("getAuthorById() GET - author with id: " + authorId + " successfuly returned");
		LOGGER.info("getAuthorById() GET - end");
		return new ResponseEntity<Author>(author, HttpStatus.OK);
	}

	@RequestMapping(value = "/categories", method = RequestMethod.GET)
	public ResponseEntity<List<Category>> getAllCategories() {
		LOGGER.info("getAllCategories() GET - start");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		List<Category> categories = categoryService.getAll();
		LOGGER.info("getAllCategories() GET - end");
		return new ResponseEntity<List<Category>>(categories, HttpStatus.OK);
	}

	@RequestMapping(value = "/category", method = RequestMethod.POST)
	public ResponseEntity<Category> createCategory(@RequestBody @Valid Category category) {
		LOGGER.info("createCategory() POST - start");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		Category createdCategory = categoryService.save(category);

		LOGGER.info("createCategory() POST - category successfuly created");
		LOGGER.info("createCategory() POST - end");
		return new ResponseEntity<Category>(createdCategory, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/category/{categoryId}", method = RequestMethod.GET)
	public ResponseEntity<Category> getCategory(@PathVariable Long categoryId) {
		LOGGER.info("getCategory() GET - start");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		Category category = categoryService.findByKey(categoryId);

		LOGGER.info("getCategory() GET - category successfuly returned");
		LOGGER.info("getCategory() GET - end");
		return new ResponseEntity<Category>(category, HttpStatus.OK);
	}

	@RequestMapping(value = "/roles", method = RequestMethod.GET)
	public ResponseEntity<List<Role>> getAllRoles() {
		LOGGER.info("getAllRoles() GET - start");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		List<Role> roles = roleService.getAll();
		LOGGER.info("getAllRoles() GET - end");
		return new ResponseEntity<List<Role>>(roles, HttpStatus.OK);
	}

}
