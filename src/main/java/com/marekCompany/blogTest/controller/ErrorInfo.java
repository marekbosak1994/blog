package com.marekCompany.blogTest.controller;

import java.sql.Timestamp;

public class ErrorInfo {
	public final Timestamp timestamp;
	public final String status;
	public final int error;
	public final String message;
	public final String path;
	
	public ErrorInfo(Timestamp time, String status, int error, String message, String path) {
		super();
		this.timestamp = time;
		this.status = status;
		this.error = error;
		this.message = message;
		this.path = path;
	}

}
