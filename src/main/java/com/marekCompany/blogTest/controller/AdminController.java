package com.marekCompany.blogTest.controller;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.marekCompany.blogTest.model.Author;
import com.marekCompany.blogTest.model.Post;
import com.marekCompany.blogTest.model.Role;
import com.marekCompany.blogTest.service.CategoryService;
import com.marekCompany.blogTest.service.PostService;
import com.marekCompany.blogTest.service.PostViewService;
import com.marekCompany.blogTest.service.RoleService;
import com.marekCompany.blogTest.validator.GroupCheck.Insert;
import com.marekCompany.blogTest.validator.GroupCheck.Last;
import com.marekCompany.blogTest.validator.GroupCheck.Update;
import com.marekCompany.blogTest.service.AuthorService;

@RestController
@RequestMapping(value = "/admin")
public class AdminController {

	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	@Autowired
	AuthorService authorService;

	@Autowired
	PostService postService;

	@Autowired
	PostViewService postViewService;

	@Autowired
	RoleService roleService;

	@Autowired
	CategoryService categoryService;

	/*@RequestMapping(value = "/home", method = RequestMethod.GET)
	public ModelAndView home() {
		LOGGER.info("home() GET - start");
		ModelAndView modelAndView = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Author author = authorService.findByUsername(auth.getName());
		modelAndView.addObject("userName", "Welcome " + author.getNickname() + " (" + author.getEmail() + ")");
		modelAndView.addObject("adminMessage", "Content Available Only for Users with Admin Role");
		modelAndView.setViewName("admin/home");
		LOGGER.info("home() GET - end");
		return modelAndView;
	}*/

	/*
	 * @RequestMapping(value = "/posts", method = RequestMethod.GET) public
	 * ResponseEntity<List<Post>> getAll() { LOGGER.info("getAll() GET - start");
	 * Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	 * 
	 * List<Post> posts = postService.getAll();
	 * 
	 * LOGGER.info("getAll() GET - end"); return new
	 * ResponseEntity<List<Post>>(posts, HttpStatus.OK); }
	 */

	@RequestMapping(value = "/getloggeduser", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Author> getLoggedUser() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Author author = authorService.findByUsername(auth.getName());
		author.setPassword(null);
		return new ResponseEntity<Author>(author, HttpStatus.OK);
	}

	@RequestMapping(value = "/posts", method = RequestMethod.GET)
	public ResponseEntity<List<Post>> getByStatus(@RequestParam(value = "status", defaultValue = "ALL") String status) {
		LOGGER.info("getByStatus() GET - start");
		LOGGER.info("getByStatus() status: " + status);
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		List<Post> posts = postService.getByStatus(status);

		LOGGER.info("getByStatus() GET - end");
		return new ResponseEntity<List<Post>>(posts, HttpStatus.OK);
	}

	@RequestMapping(value = "/posts", method = RequestMethod.POST)
	public ResponseEntity<Post> createPost(@RequestBody @Validated({ Insert.class, Last.class }) Post postToCreate) {
		LOGGER.info("createPost() POST - start");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		Post savedPost = postService.save(postToCreate);

		LOGGER.info("createPost() POST - post successfuly created");
		LOGGER.info("createPost() POST - end");
		return new ResponseEntity<Post>(savedPost, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/post", method = RequestMethod.PUT)
	public ResponseEntity<Post> updatePost(@RequestBody @Validated({ Update.class, Last.class }) Post postToUpdate) {
		LOGGER.info("updatePost() PUT - start");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		Post updatedPost = postService.update(postToUpdate);

		LOGGER.info("updatePost() PUT - post with id: " + postToUpdate.getId() + " successfuly updated by: "
				+ auth.getName());
		LOGGER.info("updatePost() PUT - end");
		return new ResponseEntity<Post>(updatedPost, HttpStatus.OK);
	}

	@RequestMapping(value = "/post/{postId}", method = RequestMethod.GET)
	public ResponseEntity<Post> getPostById(@PathVariable Long postId) {
		LOGGER.info("getPostById() GET - start");
		LOGGER.info("getPostById() GET - postid: " + postId);
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		Post post = postService.findByKey(postId);

		LOGGER.info("getPostById() GET - post with id: " + postId + " successfuly returned");
		LOGGER.info("getPostById() GET - end");
		return new ResponseEntity<Post>(post, HttpStatus.OK);
	}

	@RequestMapping(value = "/post/{postId}", method = RequestMethod.DELETE)
	public HttpStatus deletePost(@PathVariable Long postId) {
		LOGGER.info("deletePost() DELETE - start");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		postService.delete(postId);

		LOGGER.info("deletePost() DELETE - post with id: " + postId + " successfuly deleted");
		LOGGER.info("deletePost() DELETE - end");
		return HttpStatus.OK;
	}

	@RequestMapping(value = "/author", method = RequestMethod.POST)
	public ResponseEntity<Author> createAuthor(@RequestBody @Validated(Insert.class) Author authorToSave) {
		LOGGER.info("createAuthor() POST - start");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		Author savedAuthor = authorService.save(authorToSave);

		LOGGER.info("createAuthor() POST - author successfuly created");
		LOGGER.info("createAuthor() POST - end");
		return new ResponseEntity<Author>(savedAuthor, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/author", method = RequestMethod.PUT)
	public ResponseEntity<Author> updateAuthor(@RequestBody @Validated(Update.class) Author authorToUpdate) {
		LOGGER.info("updateAuthor() PUT - start");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		Author updatedAuthor = authorService.update(authorToUpdate);

		LOGGER.info("updateAuthor() PUT - author with id: " + authorToUpdate.getId() + " successfuly updated");
		LOGGER.info("updateAuthor() PUT - end");
		return new ResponseEntity<Author>(updatedAuthor, HttpStatus.OK);
	}

	@RequestMapping(value = "/author/{authorId}", method = RequestMethod.DELETE)
	public HttpStatus deleteAuthor(@PathVariable Long authorId) {
		LOGGER.info("deleteAuthor() DELETE - start");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		authorService.delete(authorId);

		LOGGER.info("deleteAuthor() DELETE - author with id: " + authorId + " successfuly deleted");
		LOGGER.info("deleteAuthor() DELETE - end");
		return HttpStatus.OK;
	}

	@RequestMapping(value = "/category/{categoryId}", method = RequestMethod.DELETE)
	public ResponseEntity<HttpStatus> deleteCategory(@PathVariable Long categoryId) {
		LOGGER.info("deleteCategory() DELETE - start");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		categoryService.delete(categoryId);

		LOGGER.info("deleteCategory() DELETE - category successfuly deleted");
		LOGGER.info("deleteCategory() DELETE - end");
		return new ResponseEntity<HttpStatus>(HttpStatus.OK);
	}

	@RequestMapping(value = "/role", method = RequestMethod.POST)
	public ResponseEntity<Role> createRole(@RequestBody @Valid Role role) {
		LOGGER.info("createRole() POST - start");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		Role createdRole = roleService.save(role);

		LOGGER.info("createRole() POST - role successfuly created");
		LOGGER.info("createRole() POST - end");
		return new ResponseEntity<Role>(createdRole, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/role/{roleId}", method = RequestMethod.DELETE)
	public ResponseEntity<HttpStatus> deleteRole(@PathVariable Long roleId) {
		LOGGER.info("deleteRole() DELETE - start");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		roleService.delete(roleId);

		LOGGER.info("deleteRole() DELETE - role successfuly deleted");
		LOGGER.info("deleteRole() DELETE - end");
		return new ResponseEntity<HttpStatus>(HttpStatus.OK);
	}

}
