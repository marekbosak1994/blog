package com.marekCompany.blogTest.controller;

import java.sql.Timestamp;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.marekCompany.blogTest.exception.NotFoundException;

import io.jsonwebtoken.ExpiredJwtException;

@ControllerAdvice
public class ExceptionController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionController.class);

	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ExceptionHandler(NotFoundException.class)
	@ResponseBody
	public ErrorInfo handleBadNotFoundRequest(HttpServletRequest req, Exception ex) {
		Timestamp time = new Timestamp(System.currentTimeMillis());
		String msg = ex.getMessage();
		String path = req.getServletPath();
		String method = req.getMethod();
		LOGGER.info(String.format("ExceptionHandler() time:%s, message: %s, path: %s, error: %d, method: %s",
				time.toString(), msg, path, HttpStatus.NOT_FOUND.value(), method));
		return new ErrorInfo(time, "Not Found", 404, msg, path);
	}

	/*@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(BadRequestException.class)
	@ResponseBody
	public ErrorInfo handleBadBadRequest(HttpServletRequest req, Exception ex) {
		Timestamp time = new Timestamp(System.currentTimeMillis());
		String msg = ex.getMessage();
		String path = req.getServletPath();
		String method = req.getMethod();
		LOGGER.info(String.format("ExceptionHandler() time:%s, message: %s, path: %s, error: 404, method: %s",
				time.toString(), msg, path, HttpStatus.BAD_REQUEST.value(), method));
		return new ErrorInfo(time, "Bad request", HttpStatus.BAD_REQUEST.value(), msg, path);
	}*/

	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(ExpiredJwtException.class)
	@ResponseBody
	public ErrorInfo handleExpiredJwtException(HttpServletRequest req, Exception ex) {
		Timestamp time = new Timestamp(System.currentTimeMillis());
		String msg = ex.getMessage();
		String path = req.getServletPath();
		String method = req.getMethod();
		LOGGER.info(String.format("ExceptionHandler() time:%s, message: %s, path: %s, error: 404, method: %s",
				time.toString(), msg, path, HttpStatus.INTERNAL_SERVER_ERROR.value(), method));
		return new ErrorInfo(time, "Internal server error", HttpStatus.INTERNAL_SERVER_ERROR.value(), msg, path);
	}
}
