package com.marekCompany.blogTest.user;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.marekCompany.blogTest.model.Author;
import com.marekCompany.blogTest.model.Role;
import com.marekCompany.blogTest.repository.AuthorRepository;


@Service("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private AuthorRepository authorRepository;

	@Override
	@Transactional
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Author author = authorRepository.findByUsername(username);
		if (author == null) {
			throw new UsernameNotFoundException(username);
		}
		Set<Role> setOfAuthorRoles = author.getRoles();

		return new User(author.getUsername(), author.getPassword(), getAuthorities(setOfAuthorRoles));
	}

	private Collection<? extends GrantedAuthority> getAuthorities(Set<Role> setOfAuthorRoles) {
		ArrayList<GrantedAuthority> listOfAuthorities = new ArrayList<>();

		for (Role role : setOfAuthorRoles) {
			listOfAuthorities.add(new SimpleGrantedAuthority(role.getRole()));
		}

		return listOfAuthorities;
	}

}
