package com.marekCompany.blogTest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.marekCompany.blogTest.model.Author;

@Repository("authorRepository")
public interface AuthorRepository extends JpaRepository<Author, Long> {
	Author findByUsername(String username);
}