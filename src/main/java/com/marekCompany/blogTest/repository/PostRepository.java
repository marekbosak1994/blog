package com.marekCompany.blogTest.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.marekCompany.blogTest.model.Post;
import com.marekCompany.blogTest.PostStatus;
import com.marekCompany.blogTest.model.Author;

@Repository("postRepository")
public interface PostRepository extends JpaRepository<Post, Long>{
	
	List<Post> findByAuthorId(Long authorId);
	List<Post> findByCategoriesId(Long categoryId);
	List<Post> findByStatusOrderByCreatedAsc(PostStatus postStatus);
	Post findByUrlName(String urlName);
}
