package com.marekCompany.blogTest.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.marekCompany.blogTest.model.Post;
import com.marekCompany.blogTest.model.PostView;

@Repository("postViewRepository")
public interface PostViewRepository extends JpaRepository<PostView, Post>{
	public int countByPost(Post postId);
	public List<PostView> findByPostId(Long postId);
	public List<PostView> findByPostAndCookieAndViewed(Post post, String cookie, Date viewed);
}
