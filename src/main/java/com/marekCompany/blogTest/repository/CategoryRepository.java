package com.marekCompany.blogTest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.marekCompany.blogTest.model.Category;

@Repository("categoryRepository")
public interface CategoryRepository extends JpaRepository<Category, Long>{
	public Category findByName(String name);
}
