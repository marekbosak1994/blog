package com.marekCompany.blogTest.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.marekCompany.blogTest.model.PostComment;

public interface PostCommentRepository extends JpaRepository<PostComment, Long>{

}
