package com.marekCompany.blogTest.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.groups.Default;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.marekCompany.blogTest.model.Author;
import com.marekCompany.blogTest.model.Post;
import com.marekCompany.blogTest.service.AuthorService;
import com.marekCompany.blogTest.service.PostService;
import com.marekCompany.blogTest.validator.GroupCheck.Insert;
import com.marekCompany.blogTest.validator.GroupCheck.Update;

public class UniquenessValidator implements ConstraintValidator<CheckUniqueness, Object> {

	private static final Logger LOGGER = LoggerFactory.getLogger(UniquenessValidator.class);

	@Autowired
	private PostService postService;

	@Autowired
	private AuthorService authorService;

	private Class<?>[] groupClass;

	@Override
	public void initialize(CheckUniqueness constraintAnnotation) {
		groupClass = constraintAnnotation.groups();
	}

	@Override
	public boolean isValid(Object value, ConstraintValidatorContext context) {
		if (value instanceof Post) {
			boolean isValidUrlName = false;
			Post postToValid = (Post) value;
			String urlName = postToValid.getUrlName();
			Long postToValidId = postToValid.getId();

			if (postToValidId != null) { // Check whether it is update existing post or inserting new one
				Post postContainsUrlName = postService.findByUrlName(urlName);
				Long postContainsId = postContainsUrlName.getId();

				isValidUrlName = postToValidId.equals(postContainsId);

			} else {
				isValidUrlName = postService.isUrlNameUnique(urlName);
			}

			context.disableDefaultConstraintViolation();
			context.buildConstraintViolationWithTemplate("Urlname already exists!").addPropertyNode("urlName")
					.addConstraintViolation();

			return isValidUrlName;

		} else if (value instanceof Author) {
			boolean isValidUserName = false;
			Author authorToValid = (Author) value;
			String username = authorToValid.getUsername();
			Long authorToValidId = authorToValid.getId();

			if (authorToValidId != null) { // Check whether it is update of existing author or insert of new one
				Author authorContainsUsername = authorService.findByUsername(username);
				Long authorContainsUsernameId = authorContainsUsername.getId();

				isValidUserName = authorToValidId.equals(authorContainsUsernameId);

			} else {
				isValidUserName = authorService.isUsernameUnique(username);
			}

			context.disableDefaultConstraintViolation();
			context.buildConstraintViolationWithTemplate("Username already exists!").addPropertyNode("username")
					.addConstraintViolation();

			return isValidUserName;
		}
		return false;

	}

}
