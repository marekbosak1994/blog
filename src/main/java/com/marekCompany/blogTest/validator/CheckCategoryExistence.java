package com.marekCompany.blogTest.validator;

import static java.lang.annotation.ElementType.FIELD;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target(FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = CategoryExistenceValidator.class)
@Documented
public @interface CheckCategoryExistence {

	String message() default "{com.marekCompany.blogTest.validator.CheckCategoryExistence" + "message}";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}
