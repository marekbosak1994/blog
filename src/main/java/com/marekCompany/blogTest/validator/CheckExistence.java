package com.marekCompany.blogTest.validator;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = ExistenceValidator.class)
@Documented
public @interface CheckExistence {

	String message() default "{com.marekCompany.blogTest.validator.CheckExistence" +
            "message}";

    Class<?>[] groups() default { };
    
    Class<? extends Payload>[] payload() default { };
    
    
}
