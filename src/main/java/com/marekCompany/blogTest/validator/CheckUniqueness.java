package com.marekCompany.blogTest.validator;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.ElementType.ANNOTATION_TYPE;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target(value = {TYPE, FIELD, ANNOTATION_TYPE} )
@Retention(value = RetentionPolicy.RUNTIME)
@Constraint(validatedBy = UniquenessValidator.class)
@Documented
public @interface CheckUniqueness {

	String message() default "{com.marekCompany.blogTest.validator.CheckUniqueness" +
            "message}";

    Class<?>[] groups() default { };
    
    Class<? extends Payload>[] payload() default { };
    
}
