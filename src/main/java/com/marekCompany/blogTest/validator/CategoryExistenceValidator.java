package com.marekCompany.blogTest.validator;

import java.util.Set;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.marekCompany.blogTest.model.Category;
import com.marekCompany.blogTest.service.CategoryService;

public class CategoryExistenceValidator implements ConstraintValidator<CheckCategoryExistence, Set<Category>> {

	@Autowired
	CategoryService categoryService;

	@Override
	public void initialize(CheckCategoryExistence constraintAnnotation) {
	}

	@Override
	public boolean isValid(Set<Category> listOfCategories, ConstraintValidatorContext context) {
		for (Category category : listOfCategories) {
			Long id = category.getId();
			if (id != null && !categoryService.exists(id)) {
				return false;
			}
		}
		return true;
	}

}
