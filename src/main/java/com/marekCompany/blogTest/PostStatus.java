package com.marekCompany.blogTest;

public enum PostStatus {
	DRAFT,
	PUBLISHED,
	ARCHIVED
}
